package dyl.sys.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.CryptTool;
import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysUser;
/**

 * @author Dyl
 * 2017-03-22 21:57:10
 */
@Service
public class SysUserServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	@Resource
	private SysRoleUserServiceImpl sysRoleUserServiceImpl;
	/**
	 * 说明：分页查询表sys_user记录封装成List集合
	 * @return JSONObject
	 */
	public List<SysUser>  findSysUserList(Page page,SysUser sysUser) throws Exception{
		String sql = "select t.*,(select "+DylSqlUtil.getRowToLine()+"(name) from sys_role t1 ,sys_role_user t2 where t1.id=t2.roleid and t2.userid = t.id) as roleNames"
				+ " from sys_user t where t.id >0";
		List<Object> con = new ArrayList<Object>();
		if(StringUtils.isNotEmpty(sysUser.getUsername())){
			sql+=" and t.username like ?";
			con.add("%"+sysUser.getUsername()+"%");
		}
		List<SysUser> sysUserList = jdbcTemplate.queryForListBeanByPage(sql, con, SysUser.class, page);
		return sysUserList;
	}
	/**
	 * 说明：根据主键查询表sys_user中的一条记录 
	 * @return SysUser
	 */
	public SysUser getSysUser(BigDecimal id) throws Exception{
		String sql = "select id,username,name,password,email,phone,state,create_time,creator from sys_user where id=?";
		SysUser sysUser  = jdbcTemplate.queryForBean(sql,new Object[]{id},SysUser.class); 
		return sysUser;
	}
	/**
	 * 说明：往表sys_user中插入一条记录
	 * @return int >0代表操作成功
	 */
	@Transactional
	public int insertSysUser(SysUser sysUser,String[] roleIds,String[] oldRoleIds,SysUser loginUser) throws Exception{
		BigDecimal userId = jdbcTemplate.queryForObject("select "+DylSqlUtil.getnextSeqNextVal()+" from dual", BigDecimal.class);
		String sql = "insert into sys_user(id,username,name,password,email,phone,state,creator) values(?,?,?,?,?,?,?,?)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{userId,sysUser.getUsername(),sysUser.getName(),sysUser.getPassword(),sysUser.getEmail(),sysUser.getPhone(),sysUser.getState(),loginUser.getCreator()});
		//判断用户角色权限是否改变
		if(!Arrays.toString(roleIds).equals(Arrays.toString(oldRoleIds))){
			sysRoleUserServiceImpl.updateSysRoleUser(sysUser.getId(),roleIds,loginUser);
		}
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表sys_user中的记录
	 * @return int >0代表操作成功
	 */
	@Transactional
	public int updateSysUser(SysUser sysUser,String[] roleIds,String[] oldRoleIds,SysUser loginUser) throws Exception{
		String sql = "update sys_user t set ";
		List<Object> con = new ArrayList<Object>();
		if(sysUser.getName()!=null){
			sql+="t.name=?,";
			con.add(sysUser.getName());
		}
		if(sysUser.getPassword()!=null){
			sql+="t.password=?,";
			con.add(sysUser.getPassword());
		}
		if(sysUser.getEmail()!=null){
			sql+="t.email=?,";
			con.add(sysUser.getEmail());
		}
		if(sysUser.getPhone()!=null){
			sql+="t.phone=?,";
			con.add(sysUser.getPhone());
		}
		if(sysUser.getState()!=null){
			sql+="t.state=?,";
			con.add(sysUser.getState());
		}
		sql=sql.substring(0,sql.length()-1);
		sql+=" where id=?";
		con.add(sysUser.getId());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		//判断用户角色权限是否改变
		if(!Arrays.toString(roleIds).equals(Arrays.toString(oldRoleIds))){
			sysRoleUserServiceImpl.updateSysRoleUser(sysUser.getId(),roleIds,loginUser);
		}
		return returnVal;
	}
	/**
	 * 说明：根据主键删除表sys_user中的记录
	 * @return int >0代表操作成功
	 */
	@Transactional
	public int deleteSysUser(BigDecimal id) throws Exception{
		String sql = "delete from sys_user where id=?";
		sysRoleUserServiceImpl.deleteUserRoles(id);//删除用户相关角色
		return jdbcTemplate.update(sql,new Object[]{id});
	}
	/**
	 * 重置密码
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public int rPwd(BigDecimal id) throws Exception{
		return jdbcTemplate.update("update sys_user set password=? where id=?",new Object[]{defaultPWD(),id});
	}
	public String defaultPWD(){
		return CryptTool.md5("123456");
	}
}
