package dyl.sys.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dyl.common.util.DylSqlUtil;
import dyl.common.util.JdbcTemplateUtil;
import dyl.common.util.Page;
import dyl.sys.bean.SysAuthKind;
/**

 * @author Dyl
 * 2017-04-14 17:37:34
 */
@Service
public class SysAuthKindServiceImpl {
	@Resource
	private JdbcTemplateUtil jdbcTemplate;
	/**
	 * 说明：分页查询表sys_kind记录封装成List集合
	 * @return List<SysAuthKind
	 */
	public List<SysAuthKind>  findSysAuthKindList(Page page,SysAuthKind sysAuthKind) throws Exception{
		String sql = "select id,name from sys_kind t where 1=1";
		List<Object> con = new ArrayList<Object>();
		if(sysAuthKind.getId()!=null){
			sql+=" and t.id=?";
			con.add(sysAuthKind.getId());
		}
		if(StringUtils.isNotEmpty(sysAuthKind.getName())){
			sql+=" and t.name like ?";
			con.add("%"+sysAuthKind.getName()+"%");
		}
		List<SysAuthKind> sysAuthKindList =  jdbcTemplate.queryForListBeanByPage(sql, con, SysAuthKind.class, page);
		
		return sysAuthKindList;
	}
	/**
	 * 说明：根据主键查询表sys_kind中的一条记录 
	 * @return SysAuthKind
	 */
	public SysAuthKind getSysAuthKind(BigDecimal  id) throws Exception{
		String sql = "select id,name from sys_kind where id=?";
		SysAuthKind sysAuthKind  =  jdbcTemplate.queryForBean(sql, new Object[]{id},SysAuthKind.class); 
		return sysAuthKind;
	}
	/**
	 * 说明：往表sys_kind中插入一条记录
	 * @return int >0代表操作成功
	 */
	@Transactional
	public int insertSysAuthKind(SysAuthKind sysAuthKind) throws Exception{
		Integer id = 0;
		if(DylSqlUtil.isMYSQL){
			 id = jdbcTemplate.queryForInt("select max(id) + 1 from sys_kind");
		}else if(DylSqlUtil.isORACLE){
			 id = jdbcTemplate.queryForInt("select max(id) + 1 from sys_kind from dual");
		}
		
		String sql = "insert into sys_kind(id,name) values(?,?)";
		int returnVal=jdbcTemplate.update(sql,new Object[]{id,sysAuthKind.getName()});
		return returnVal;
	}
	/**
	 * 说明：根据主键更新表sys_kind中的记录
	 * @return int >0代表操作成功
	 */
	@Transactional
	public int updateSysAuthKind(SysAuthKind sysAuthKind) throws Exception{
		String sql = "update sys_kind t set ";
		List<Object> con = new ArrayList<Object>();
			if(sysAuthKind.getName()!=null){
				sql+="t.name=?,";
				con.add(sysAuthKind.getName());
			}
		sql=sql.substring(0,sql.length()-1);
		sql+=" where id=?";
		con.add(sysAuthKind.getId());
		int returnVal=jdbcTemplate.update(sql,con.toArray());
		return returnVal;
	}
	/**
	 * 说明：根据主键删除表sys_kind中的记录
	 * @return int >0代表操作成功
	 */
	@Transactional
	public int deleteSysAuthKind(String  id) throws Exception{
		String sql = "delete from  sys_kind where id=?";
		jdbcTemplate.update(sql,new Object[]{id});
		return jdbcTemplate.update("delete from sys_menu_kind where kind_id = ?",new Object[]{id});
	}
	/**
	 * 获取菜单绑定的权限Id
	 * @param menuId
	 * @return
	 * @throws Exception
	 */
	public List<SysAuthKind>  findSysMenuKindList(BigDecimal menuId) throws Exception{
		String sql = "select t1.*,t2.id as ck from sys_kind t1 left join (select * from sys_menu_kind where menu_id = ?) t2  on t1.id=t2.kind_id order by t1.id";
		List<SysAuthKind> sysAuthKindList =  jdbcTemplate.queryForListBean(sql,new Object[]{menuId},SysAuthKind.class);
		return sysAuthKindList;
	}
	@Transactional
	public int[] updateSysMenuKind(BigDecimal menuId,String[] kind){
		//1.清空菜单原有权限
		jdbcTemplate.update("delete from sys_menu_kind where menu_id = ?",new Object[]{menuId});
		//2.批量更新菜单权限
		if(kind!=null){
			String sql = "insert into sys_menu_kind(id,menu_id,kind_id) values("+DylSqlUtil.getnextSeqNextVal()+",?,?)";
			List<Object[]> para = new ArrayList<Object[]>();
			for (int i = 0; i < kind.length; i++) {
				para.add(new Object[]{menuId,kind[i]});
			}
			 jdbcTemplate.batchUpdate(sql,para);
		}
		return null;	
	}
}
